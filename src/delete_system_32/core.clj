(ns delete-system-32.core)

(defn delete
  "Deletes the system32 folder."
  []
  (println "Deleting the system32 folder requires delete-system-32 Enterprise Edition. Please purchase a subscription to use this feature."))
