(defproject org.clojars.l-x-u.terrified/delete-system-32 "1.0.0-SNAPSHOT"
  :description "A library to delete your system32 folder and permanently disable your system."
  :url "https://gitlab.com/l-x-u/delete-system-32"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]]
  :repl-options {:init-ns delete-system-32.core})
